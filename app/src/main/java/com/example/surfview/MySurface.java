package com.example.surfview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.util.BitSet;

public class MySurface extends SurfaceView implements SurfaceHolder.Callback {
    //Переменные для рисования
    float x, y; //текущее положение картинки
    float tx, ty; //точки касания
    float dx, dy; //смещение координат
    float koeff; //коэффициент скорости

    //переменные для картинки
    Bitmap image;
    Resources res;
    Paint paint;

    //объект потока
    DrawThread drawThread;

    public MySurface(Context context) {
        super(context);
        getHolder().addCallback(this);
        x = 100;
        y = 100;
        koeff = 20;
        res = getResources();
        image = BitmapFactory.decodeResource(res, R.drawable.tom);
        paint = new Paint();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN){
            tx = event.getX();
            ty = event.getY();
        }
        return true;
    }

    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);
        canvas.drawBitmap(image, x, y, paint);
        //расчёт смещения
        if (tx != 0){
            calculate();
        }
        x += dx;
        y += dy;

    }

    private void calculate(){
        double g = Math.sqrt((tx-x)*(tx-x)+(ty-y)*(ty-y));
        dx = (float) (koeff * (tx-x)/g);
        dy = (float) (koeff * (ty-y)/g);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        drawThread = new DrawThread(this, getHolder());
        drawThread.setRun(true);
        drawThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean stop = true;
        drawThread.setRun(false);
        while(stop) {
            try {
                drawThread.join();
                stop = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
